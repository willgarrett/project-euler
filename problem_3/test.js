var prob3 = require("./problem3");

test('should return 29 for 13195', () => {
    expect(prob3(13195)).toEqual(29);
});


test('should return 6857 for 600851475143', () => {
    expect(prob3(600851475143)).toEqual(6857);
});