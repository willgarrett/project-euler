function problem3(target){
    let i = 2;
    while(i < target){
        while(target%i == 0){
            (function(newtarget) {
                target = newtarget;
            })(target / i);
        }
        i++;
    }
    return target;
}
module.exports = problem3;