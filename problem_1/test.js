var prob1 = require("./problem1");
test('should return 23', () => {
    expect(prob1(10)).toEqual(23);
});

test('should return large num', () => {
    expect(prob1(1000)).toEqual(233168);
});