## Project Euler Solutions Repository
[Project Euler Problems](https://projecteuler.net/archives)

### Instructions
Install jest globally (if not already installed)

`npm i -g jest`

Run Jest
`jest`