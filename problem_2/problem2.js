function problem2(constraint){
    let fib_a = 0;
    let fib_b = 1;
    let sum = 0;
    while(fib_b <= constraint){
        let temp = fib_a + fib_b;
        fib_a = fib_b;
        fib_b = temp;
        if(temp % 2 == 0){
            sum+= fib_b;
        }
    }
    return sum;
}   
module.exports = problem2;